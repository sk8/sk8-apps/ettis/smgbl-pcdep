source("global.R")


ui <- page_sidebar(
    title = div(logo, uiOutput("maj_date")),
    window_title = "PC départemental (SMGBL)",
    theme = theme,
    sidebar = sidebar(
        use_font("open-sans", "www/css/open-sans.css"),
        shinyjs::useShinyjs(),
        tags$style(HTML(".card-img-top {
    width: 100%;
}")),
        tags$style(HTML(".modal-open .container-fluid, .modal-open  .container {
    -webkit-filter: blur(5px) grayscale(90%);
}")),
        width = 350,
        title = "Configuration",
        selectInput("poste", label = "Choisir un poste", choices = c("Tous" = "0", "Biscarrosse - Le Vivier" = "1", "Mimizan - La Sud" = "2", "Contis Plage" = "3", "La Lette Blanche - Vielle Saint Girons" = "4", "Messanges Nord" = "5", "Soustons Plage" = "6", "Hossegor - La Centrale" = "7", "Hossegor - La Sud" = "8", "La Savane - Cap Breton" = "9", "Ondres Plage" = "10", "Le Metro - Tarnos" = "11")),
        dateRangeInput("dates", label = "Choisir une période", language = "fr", separator = "à", start = "2024-06-15", end = lubridate::today()),
        hr(),
        h5("A propos de cette application"),
        p("Cette application est réalisée dans le cadre du projet IRICOT financé par le PEPR IRIMA (dans la suite du projet SWYM) et permet une visualisation interactive des saisies réalisées à partir de l'outil ODKCollect proposé par INRAE."),
        markdown("Pour plus d'informations, vous pouvez contacter [David Carayon](mailto:david.carayon@inrae.fr) (Développeur) ou [Jeoffrey Dehez](mailto:jeoffrey.dehez@inrae.fr) (Coordinateur).")
    ),
    navset_bar(
        nav_panel(
            title = "Accueil", icon = icon("house"),
            card(
                card_title("Données clés"),
                card_body(
                    layout_columns(
                        value_box(
                            title = "Poste", value = textOutput("nom_poste") |> shinycssloaders::withSpinner(), , theme = NULL,
                            showcase = fontawesome::fa_i("house-medical-flag"), showcase_layout = "left center",
                            full_screen = FALSE, fill = TRUE, height = NULL
                        ),
                        value_box(
                            title = "Nombre d’opérations", value = textOutput("nb_interv") |> shinycssloaders::withSpinner(), ,
                            theme = NULL, showcase = fontawesome::fa_i("watchman-monitoring"),
                            showcase_layout = "left center", full_screen = FALSE, fill = TRUE,
                            height = NULL
                        ),
                        value_box(
                            title = "Nombre de personnes concernées", value = textOutput("nb_pers") |> shinycssloaders::withSpinner(), ,
                            theme = NULL, showcase = fontawesome::fa_i("users"),
                            showcase_layout = "left center", full_screen = FALSE, fill = TRUE,
                            height = NULL
                        )
                    )
                ),
                fill = FALSE
            ),
            card(
                card_title("Financeurs et partenaires"),
                full_screen = TRUE,
                fill = FALSE,
                card_image(
                    file = "www/bandologo.svg",
                    width = "100%",
                    href = "https://www.projet-swym.fr/data.interventions"
                )
            )
        ),
        nav_panel(
            title = "Répartition journalière", icon = shiny::icon("clock"),
            card(
                card_title(div("Répartition journalière des interventions", tags$a("   Comment ça marche ?", class = "fs-6 fw-lighter text-primary") |> popover(markdown(
                    "Vous pouvez sélectionner dans le sous-menu de gauche les types d'interventions à faire apparaître. Il est possible de commencer à taper un type d'intervention pour lancer une recherche sur la liste. L'option `Toutes` (défaut) permet d'afficher toutes les interventions, l'ajout d'autres types n'aura pas d'impact tant que `Toutes` est sélectionné."
                ), placement = "right"))),
                card_body(
                    layout_sidebar(
                        sidebar = sidebar(
                            id = "journal_id",
                            selectInput("sel_journalier", "Choisir le type d'interventions", choices = c("Secours en mer" = "inter_sec_mer", "Personnes disparues" = "inter_disparue", "Découverte Corps" = "inter_corps", "Patrouille gendarmes" = "gendarmerie", "Préventions" = "prevention", "Aide à baigneur" = "aide_baigneur", "Aide à navigateur" = "aide_navigateur", "Aide à surfeur, véliplanchiste et autre" = "aide_surfeur", "Noyade (Stade 1)" = "noyade_1", "Petit soins" = "petit_soins", "Piqure Physalie" = "physalie", "Malaise" = "malaise", "Traumatisme" = "trauma", "Noyade principale (Stade 2+)" = "noyade", "Noyade secondaire (Stade 2+)" = "noyade_sec", "Plaie" = "plaie", "Toutes" = "Toutes"), selected = "Toutes", multiple = TRUE),
                            downloadButton("dl_graph_journalier", "Télécharger ce graphique", class = "btn-success")
                        ),
                        ggiraph::girafeOutput("journalier", width = "100%") |> shinycssloaders::withSpinner()
                    ),
                    fill = TRUE
                ),
                fill = TRUE,
                fillable = FALSE,
            )
        ),
        nav_panel(
            title = "Statistiques", icon = icon("magnifying-glass-chart"),
            card(
                full_screen = TRUE,
                fill = FALSE,
                card_title(div("Explorateur", tags$a("   Comment ça marche ?", class = "fs-6 fw-lighter text-primary") |> popover(markdown(
                    "* Les graphiques et tableaux peuvent être téléchargés via le bouton vert en dessous de ceux-ci
                    * Les tableaux peuvent être copiés-collés directement dans word ou excel.
                    * La zone de visualisation des graphiques/tableaux peut également être agrandie via le bouton 'expand' en bas à droite"
                ), placement = "right"))),
                card_body(
                    fillable = FALSE,
                    layout_columns(
                        selectInput("varsel", "Choisir la statistique à afficher", choices = c("Toutes les interventions" = "type_interv", "Traumatismes" = "trauma", "Plaies graves" = "plaies"), selected = "nature_interv"),
                        radioButtons("output_type", "Choisir le format", choices = c("Graphique", "Tableau"), inline = TRUE, selected = "Graphique"),
                        input_switch("include_loc", "Croiser avec la localisation"),
                        # radioButtons("intervpers", "Choisir le N", choices = c("Interventions", "Personnes"), inline = TRUE, selected = "Interventions"),
                    ),
                    conditionalPanel(
                        condition = "input.output_type == 'Graphique'",
                        ggiraph::girafeOutput("plot", width = "100%") |> shinycssloaders::withSpinner(),
                        downloadButton("dl_graph", "Télécharger ce graphique", class = "btn-success")
                    ),
                    conditionalPanel(
                        condition = "input.output_type == 'Tableau'",
                        gt::gt_output("table") |> shinycssloaders::withSpinner(),
                        downloadButton("dl_table", "Télécharger ce tableau", class = "btn-success")
                    )
                )
            )
        ),
        # nav_panel()
        # nav_panel(
        #     title = "Répartion horaire", icon = icon("clock"),
        #     card(
        #         fill = TRUE, full_screen = TRUE,
        #         cart_title = "Répartition horaire",
        #         card_body(
        #             layout_sidebar(
        #                 fillable = FALSE,
        #                 sidebar = sidebar(
        #                     selectInput("sel_horaire", "Choisir le type d'interventions", choices = c("Toutes" = "toutes", "Préventions" = "prev", "Aides à [...]" = "aides", "Noyades" = "noyades", "Malaises, traumatismes, petits soins, physalies" = "malaises_trauma"), selected = "toutes")
        #                 ),
        #                 ggiraph::girafeOutput("horaire", width = "100%") |> shinycssloaders::withSpinner(),
        #                 downloadButton("dl_graph_horaire", "Télécharger ce graphique", class = "btn-success")
        #             )
        #         )
        #     )
        # ),

        nav_panel(
            title = "Fichiers joints", icon = icon("file-pdf"),
            card(
                fill = FALSE,
                card_title(div("Liste des fichiers joints pour du secours en mer", tags$a(shiny::icon("warning")) |> popover(markdown(
                    "Attention : Cette liste ne tient pas compte des filtres de poste / date appliqués, elle concerne l'intégralité des interventions de la saison"
                ), placement = "right"))),
                card_body(
                    height = "400px",
                    fill = TRUE,
                    div(
                        selectInput("sel_file", "Choisir l'intervention", choices = NULL, width = "40%"),
                        downloadButton("dl_file", "Télécharger", class = "btn-success", width = "40%")
                    )
                )
            )
        ),
        nav_spacer(),
        nav_panel(
            title = "Cartographie", icon = icon("map-location-dot"),
            card(
                fill = TRUE,
                full_screen = TRUE,
                card_title(div("Cartographie provisoire (tous les postes)", tags$a(shiny::icon("warning")) |> popover(markdown(
                    "Attention : Cette représentation graphique ne tient pas compte des filtres de poste / date appliqués, elle concerne l'intégralité des interventions de la saison"
                ), placement = "right"))),
                card_body(leafletOutput("map", height = 700))
            )
        ),
        nav_panel(
            title = "Données brutes", icon = icon("database"),
            card(
                card_title(div("Données brutes  (tous les postes)", tags$a(shiny::icon("warning")) |> popover(markdown(
                    "Attention : Ces données ne tiennent pas compte des filtres de poste / date appliqués, elles concernent l'intégralité des interventions de la saison"
                ), placement = "right"), downloadButton("dl_database", "Télécharger la base de données", icon = icon("download"), class = "btn-success float-right"))),
                card_body(
                    reactableOutput("data_table") |> shinycssloaders::withSpinner()
                )
            )
        ),
        nav_panel(
            title = "A propos", icon = icon("circle-info"),
            card(
                title = "a propos",
                card_body(
                    includeMarkdown("www/intro.md")
                )
            )
        ),
        nav_item(input_dark_mode(id = "dark"))
    )
)

server <- function(input, output, session) {
    showModal(
        modalDialog(
            title = div("Bonjour", shiny::icon("sun")),
            size = "xl",
            footer = "",
            div(
                p("Merci de cliquer ci-dessous pour lancer l'application et la collecte des données des formulaires :")
            ),
            br(),
            div(
                class = "text-center",
                input_task_button("run", "Démarrer l'application",
                    icon = icon("play"), label_busy = "En cours...", type = "success", class = "text-center"
                )
            )
        )
    )


    observeEvent(input$run, {
        update_database()
        intervs <- request_data_file()
        updateDateRangeInput(session, "dates", end = lubridate::today())
        updateSelectInput(session = session, "sel_file", choices = intervs$contexte)

        removeModal()
    })

    output$maj_date <- renderUI({
        req(input$run)
        p(glue("Mise à jour : \n{format(lubridate::now(),'%Y-%m-%d %H:%M:%S')}"), class = "fs-6 fw-lighter text-muted")
    })

    output$plot <- ggiraph::renderGirafe({
        req(input$run)
        if (input$varsel == "type_interv") {
            dat <- request_data_interv(localisation = input$include_loc, poste = input$poste, dates = input$dates)
            p <- plot_interv(dat, localisation = input$include_loc)
        } else if (input$varsel == "trauma") {
            dat <- request_data_trauma(localisation = input$include_loc, poste = input$poste, dates = input$dates)
            p <- plot_accidents(dat, localisation = input$include_loc)
        } else if (input$varsel == "plaies") {
            dat <- request_data_plaies(localisation = input$include_loc, poste = input$poste, dates = input$dates)
            p <- plot_accidents(dat, localisation = input$include_loc)
        } else {
            dat <- request_data_reanimations(localisation = input$include_loc, poste = input$poste, dates = input$dates)
            p <- plot_reanimation(dat, localisation = input$include_loc)
        }

        ggiraph::girafe(print(p), width_svg = 18, height_svg = 9, options = list(opts_sizing(rescale = TRUE)))
    })



    output$nom_poste <- renderText({
        req(input$run)
        dictionnaire[[input$poste]]
    })

    output$nb_pers <- renderText({
        req(input$run)
        get_nb_pers(input$poste, input$dates)
    })


    output$nb_interv <- renderText({
        req(input$run)
        length(get_keys(input$poste, input$dates))
    })

    output$journalier <- renderGirafe({
        req(input$run)
        dat <- request_data_journalier(input$poste, input$dates, input$sel_journalier)
        p <- plot_journalier(dat, start = input$dates[1], end = input$dates[2])
        ggiraph::girafe(print(p), width_svg = 18, height_svg = 9, options = list(opts_sizing(rescale = TRUE)))
    })

    output$horaire <- renderGirafe({
        req(input$run)
        dat <- request_data_horaire(input$poste, input$dates, input$sel_horaire)
        p <- plot_horaire(dat)
        ggiraph::girafe(print(p), width_svg = 18, height_svg = 9, options = list(opts_sizing(rescale = TRUE)))
    })



    output$table <- gt::render_gt({
        req(input$run)
        if (input$varsel == "type_interv") {
            dat <- request_data_interv(input$include_loc, poste = input$poste, dates = input$dates)
        } else if (input$varsel == "trauma") {
            dat <- request_data_trauma(input$include_loc, poste = input$poste, dates = input$dates)
        } else if (input$varsel == "plaies") {
            dat <- request_data_plaies(input$include_loc, poste = input$poste, dates = input$dates)
        } else {
            dat <- request_data_reanimations(input$include_loc, poste = input$poste, dates = input$dates)
        }

        if (input$include_loc) {
            dat |>
                uncount(n) |>
                rename("Variable" = "var") |>
                tbl_summary(
                    include = Variable, by = localisation,
                    statistic = list(all_categorical() ~ "{n}")
                ) |>
                modify_header(label ~ "") |>
                as_gt() |>
                gt::tab_options(table.width = gt::pct(100))
        } else {
            dat |>
                uncount(n) |>
                rename("Variable" = "var") |>
                tbl_summary(
                    include = Variable,
                    statistic = list(all_categorical() ~ "{n}")
                ) |>
                modify_header(label ~ "") |>
                as_gt() |>
                gt::tab_options(table.width = gt::pct(100))
        }
    })


    rv <- reactiveValues()

    observe({
        invalidateLater(1000 * 60 * 60 * 6)
        rv$today <- Sys.Date()
        updateDateRangeInput(session, "dates", end = rv$today)
    })




    output$data_table <- renderReactable({
        req(input$run)

        database <- request_full_database()

        reactable(database,
            searchable = TRUE, filterable = TRUE, defaultPageSize = 20,
            striped = T, bordered = T, compact = T, wrap = FALSE, resizable = TRUE,
            theme = reactableTheme(
                color = "#1a4edc", borderColor = "#dfe2e5",
                highlightColor = "#f0f5f9",
                # stripedColor = "#f6f8fa",
                cellPadding = "8px 12px",
                searchInputStyle = list(width = "100%")
            )
        )
    })


    output$dl_table <- downloadHandler(
        filename = function() {
            paste(input$varsel, "-", Sys.Date(), ".xlsx", sep = "")
        },
        content = function(con) {
            withProgress(message = "Production en cours", value = 0, {
                shiny::setProgress(value = 0.2, message = "Lancement des requêtes")
                if (input$varsel == "type_interv") {
                    dat <- request_data_interv(input$include_loc, poste = input$poste, dates = input$dates)
                } else if (input$varsel == "trauma") {
                    dat <- request_data_trauma(input$include_loc, poste = input$poste, dates = input$dates)
                } else if (input$varsel == "plaies") {
                    dat <- request_data_plaies(input$include_loc, poste = input$poste, dates = input$dates)
                } else {
                    dat <- request_data_reanimations(input$include_loc, poste = input$poste, dates = input$dates)
                }

                shiny::setProgress(value = 0.4, message = "Mise en forme des tableaux")

                if (input$include_loc) {
                    tb <- dat |>
                        mutate(localisation = str_remove_all(localisation, "\n")) |>
                        pivot_wider(id_cols = var, names_from = localisation, values_from = n) |>
                        rename("Variable" = "var")
                } else {
                    tb <- dat |>
                        rename("Variable" = "var", "N" = "n")
                }


                shiny::setProgress(value = 0.7, message = "Ecriture du fichier")
                openxlsx2::write_xlsx(tb, file = con, as_table = TRUE, na.strings = "")
            })
        }
    )

    output$dl_database <- downloadHandler(
        filename = function() {
            paste("donnees_brutes", Sys.Date(), ".xlsx", sep = "")
        },
        content = function(con) {
            withProgress(message = "Production en cours", value = 0, {
                shiny::setProgress(value = 0.2, message = "Lancement des requêtes")
                dat <- request_full_database()
                shiny::setProgress(value = 0.7, message = "Ecriture du fichier")
                openxlsx2::write_xlsx(dat, file = con, as_table = TRUE, na.strings = "")
            })
        }
    )


    output$dl_file <- downloadHandler(
        filename = function() {
            file <- request_data_file()
            file$file_mer
        },
        content = function(con) {
            file <- request_data_file()
            file.copy(from = file.path("data", "media", file$file_mer), to = con)
        }
    )

    output$dl_graph <- downloadHandler(
        filename = function() {
            paste(input$varsel, "-", Sys.Date(), ".png", sep = "")
        },
        content = function(con) {
            withProgress(message = "Production en cours", value = 0, {
                shiny::setProgress(value = 0.2, message = "Lancement des requêtes")
                if (input$varsel == "type_interv") {
                    dat <- request_data_interv(localisation = input$include_loc, poste = input$poste, dates = input$dates)
                    shiny::setProgress(value = 0.2, message = "Production des graphiques")
                    p <- plot_interv(dat, localisation = input$include_loc)
                } else if (input$varsel == "trauma") {
                    dat <- request_data_trauma(localisation = input$include_loc, poste = input$poste, dates = input$dates)
                    shiny::setProgress(value = 0.2, message = "Production des graphiques")
                    p <- plot_accidents(dat, localisation = input$include_loc)
                } else if (input$varsel == "plaies") {
                    dat <- request_data_plaies(localisation = input$include_loc, poste = input$poste, dates = input$dates)
                    shiny::setProgress(value = 0.2, message = "Production des graphiques")
                    p <- plot_accidents(dat, localisation = input$include_loc)
                } else {
                    dat <- request_data_reanimations(localisation = input$include_loc, poste = input$poste, dates = input$dates)
                    shiny::setProgress(value = 0.2, message = "Production des graphiques")
                    p <- plot_reanimation(dat, localisation = input$include_loc)
                }
                shiny::setProgress(value = 0.8, message = "Sauvegarde et export")
                ggsave(plot = p, filename = con, dpi = "retina", width = 18, height = 9)
            })
        }
    )

    output$dl_graph_journalier <- downloadHandler(
        filename = function() {
            paste("journalier-", Sys.Date(), ".png", sep = "")
        },
        content = function(con) {
            withProgress(message = "Production en cours", value = 0, {
                shiny::setProgress(value = 0.2, message = "Lancement des requêtes")

                dat <- request_data_journalier(input$poste, input$dates, input$sel_journalier)

                shiny::setProgress(value = 0.6, message = "Production des graphiques")

                p <- plot_journalier(dat, start = input$dates[1], end = input$dates[2])

                shiny::setProgress(value = 0.8, message = "Sauvegarde et export")
            })

            ggsave(plot = p, filename = con, dpi = "retina", width = 18, height = 9)
        }
    )

    output$dl_graph_horaire <- downloadHandler(
        filename = function() {
            paste("horaire-", Sys.Date(), ".png", sep = "")
        },
        content = function(con) {
            dat <- request_data_horaire(input$poste, input$dates, input$sel_horaire)
            p <- plot_horaire(dat)
            ggsave(plot = p, filename = con, dpi = "retina", width = 18, height = 9)
        }
    )

    output$map <- renderLeaflet({
        req(input$run)
        `Nombre d'interventions` <- request_map_data() |>
            mutate(lab = paste0(poste_name, " : ", `Nombre d'interventions`, " interventions"))
        mapview(`Nombre d'interventions`, cex = "Nombre d'interventions", label = "lab")@map
    })
}

shinyApp(ui, server)
