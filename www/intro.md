Cette application est développée par l’unité ETTIS (Environnement, Territoire en Transition, Infrastructure et Société) du centre INRAE Nouvelle Aquitaine Bordeaux, conjointement à la mise en place d’un protocole inédit de collecte de données sur les opérations de prévention, de secours et de sauvetage réalisées par les sauveteurs du [Syndicat Mixte de Gestion des Baignades Landaises](https://smgbl.fr), durant l’été 2024.

La démarche poursuit le projet pilote initié en 2023 dans le cadre du projet SWYM (Surf zone hazards, recreational beach use and Water safetY) en partenariat avec l’Université de Bordeaux et le CNRS. Elle s’inscrit dans le cadre du projet IRICOT intégré au PEPR Risques (IRiMa- gestion intégrée des risques pour des sociétés plus résilientes à l'ère des changements globaux).

L’application  a pour objectif de fournir une série d’indicateurs visuels et synthétiques représentatifs des volumes d’interventions sur les postes ayant pris part au projet.

Pour toute question ou demande d'informations, vous pouvez contacter [Jeoffrey Dehez](mailto:jeoffrey.dehez@inrae.fr) (Coordinateur) et/ou [David Carayon](mailto:david.carayon@inrae.fr) (Développeur). 
